(function( $ ) {

    var grizzly = {};
    (function() {

        this.clickTouch = ( $('html').hasClass('touch') ) ? 'touchend' : 'click';
        this.addthis_loaded = false;
        this.scrolling = false;
        this.tabletBreakpoint = '800px';
        this.mobileQuery = 'screen and (max-width: ' + grizzly.tabletBreakpoint + ')';
        this.tabletQuery = 'screen and (min-width: ' + grizzly.tabletBreakpoint + ')';
        this.ajaxObject = {};
        this.ajaxDelay = 200;

        this.modernizerChecks = function() {
            if(Modernizr.input.placeholder) {
                $('html').addClass('placeholder');
            }
        };

        this.touchScroll = function() {
            $(window).on('touchmove', function() {
                grizzly.scrolling = true;
            }).on('touchend', function() {
                    var scrollEnableTO = window.setTimeout(function() {
                        grizzly.scrolling = false;
                    }, 100);
                });
        };

        this.touchState = function() {
            if ('ontouchstart' in document.documentElement) {
                $(document).on('touchstart', 'a:not(.product-thumb), .touch', function (e) {
                    var self = this;
                    this.timeout = setTimeout(function () {
                        $(self).addClass('active');
                    }, 50);
                }).on('touchmove', 'a:not(.product-thumb), .touch', function (e) {
                        clearTimeout(this.timeout);
                        $(this).removeClass('active');
                    }).on('touchend', 'a:not(.product-thumb), .touch', function (e) {
                        clearTimeout(this.timeout);
                        var self = this;
                        this.timeout = setTimeout(function () {
                            $(self).removeClass('active');
                        }, 100);
                    });
            } else {
                $(document).on('mousedown', 'a:not(.product-thumb), .touch', function (e) {
                    $(this).addClass('active');
                    var self = this;
                    function mouseup () {
                        $(document).off('mouseup', mouseup);
                        $(self).removeClass('active');
                    };
                    $(document).on('mouseup', mouseup);
                });
            }
        };

        this.toggleMobileMenu = function() {
            var $links = $("[data-target='menu-toggle']").next('ul').children('li');

            // Toggle controler classes
            $("[data-target='menu-toggle']").toggleClass('active');
            $("[data-target='header']").toggleClass("mobile-menu-open");

            // If menu is open it sets the max-height for the css transition.
            if( $("[data-target='menu-toggle']").hasClass('active') ) {
                $("[data-target='menu-toggle']").next('ul').css({
                    'max-height' : ($links.size() * $links.outerHeight()) + 'px'
                });
                // Otherwise it clears the style attr so the menu will close.
            } else {
                $("[data-target='menu-toggle']").next('ul').attr('style', '');
            }
        };

        this.homepageSlider = function() {
            var present = ( $('.js-homepage-slider').length > 0 );

            var sliderSetup = function() {
                $('.js-homepage-slider').flexslider({
                    animation: "slide",
                    animationLoop: true,
                    slideshow: false,
                    useCSS: false,
                    controlsContainer: ".custom-control-nav",
                    directionNav: false
                });


                $(document).on('scroll', function() {
                    var offset = $(document).scrollTop();
                    var navoffset = 0;

                    if ($('.mobile-site-header').hasClass('mobile-site-header-fixed')) {
                        navoffset = $('.panel-one').offset().top;
                    }else {
                        navoffset = $('.mobile-site-header').offset().top;
                    }


                    if (offset > 0) {
                        $('.js-homepage-slider').css({'z-index' : -1,
                            'webkit-transform' : 'translate(0px, '+offset+'px)'});

                        $('.custom-control-nav').addClass('custom-control-nav-hidden');
                    }else {
                        $('.js-homepage-slider').css({'z-index' : 0,
                            'webkit-transform' : 'translate(0px, 0px)'});
                        $('.custom-control-nav').removeClass('custom-control-nav-hidden');
                    }

                    if (offset >= navoffset) {
                        $('.mobile-site-header').addClass('mobile-site-header-fixed');
                    }else {
                        $('.mobile-site-header').removeClass('mobile-site-header-fixed');
                    }

                });
            };

            if(present) {
                sliderSetup();
            }
        };

        this.instagramFeed = function() {
            $("#instagram-feed").jqinstapics({
                "user_id": "9416756",
                "access_token": "9416756.674061d.b61b721b7e244ebfa936f2e064ba733a",
                "count": 6
            });
        };

        this.appInit = function() {
            grizzly.modernizerChecks();
            grizzly.touchScroll();

            grizzly.homepageSlider();
            grizzly.instagramFeed();
        };

    }).apply( grizzly );

    $(document).ready(function() {
        /Mobile/.test((navigator.userAgent) && !location.hash && setTimeout(function () {
            if (!window.pageYOffset) { window.scrollTo(0, 1); }
        }, 0));

        grizzly.appInit();


        // Check if menu toggle link is there
        if( $("[data-target='menu-toggle']").length > 0 ) {
            $('body').on('click.mobileMenu', function(e) {
                var clickedOn = $(e.target);
                // If clicked on the right link... toggle the form down
                if ( clickedOn.parents().andSelf().is("[data-target='menu-toggle']") ) {
                    grizzly.toggleMobileMenu();
                    // If header is open and clicked somewhere off the header... toggle the form back up
                } else if( $("[data-target='header']").hasClass('mobile-menu-open') && !clickedOn.parents().andSelf().is("[data-target='header']") ) {
                    grizzly.toggleMobileMenu();
                }
            });
        }

    }); // $document.ready
}(jQuery));
