# Grizzly Front End Dev Evaluation

This evaluation is meant to explore your techniques and code used in the construction of a very graphically simple website. In effect we are looking for a properly styled and coded responsive realization of the included comp using only the files made available. All design elements are css-generated with the exception of the instagram feed. Please make note that attention to detail is paramount in this excercise. The following list of specific tecnhiques requested:

#### Files and languages
- The evaluation should be built using the system set up within this repo.
- All styles should be created using SCSS. Dealing with breakpoints is up to you, so long as it works and is mobile-first. 
- Lorum Ipsum should be used for text content and fillmurray.com should be used for background images/slider images. 
- FontAwesome should be used for social icons
- Finally, the Instagram grid should pull a real instagram feed. If an API key is needed, you may use your own feed, a dummy feed or our test feed (username: **`grizzlytest`** and password: **`GZT3st`**)


#### Image slider
Simple slideshow using Flexslider with 3 slides. Heading & text should have subtle animate and/or fade in.

#### Navigation
Nav and rest of page should “cover” feature slider as you scroll. Nav should stick and shrink in height when it reaches top.

#### Main Panel w/ Info & graphically
Panel with left text and image on right. Text/image ahould degrade gracefully. 

#### Centered Panel
Heading, paragraph text & button should center horizontally and vertically.

#### Contact Form
Contact form design should be consistent across all platforms to the extent of cross- browser/device compatability. 

#### Form Styling
Form should be styled as close as possible to the exact flat style shown here, not using the default browser/device styling.

#### Instagram feed
Dynamically displays the latest six images from an Instagram account.

#### Nav
Nav and rest of page should “cover” feature slider as you scroll. Nav should stick and shrink in height when it reaches top.