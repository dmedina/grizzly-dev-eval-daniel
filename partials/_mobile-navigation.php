<header class="mobile-site-header">
    <div class="width-restrict">
        <a href="/" class="_logo"><span class="visuallyhidden">Grizzly</span></a>

        <nav class="m-main-navigation">
            <a data-target="menu-toggle" class="menu-toggle" href="javascript:void(0);"><i class="fa fa-align-justify fa-3x"></i></a>
            <ul>
                <!-- Output like this to avoid 4px gap put in when using inline-block -->
                <li><a href="#">Services</a></li
                    ><li><a href="#">Portfolio</a></li
                    ><li><a href="#">Customers</a></li
                    ><li><a href="#">About Us</a></li
                    ><li><a href="#">Contact</a></li>
            </ul>
        </nav>
        <div class="clear"></div>
    </div>
</header>