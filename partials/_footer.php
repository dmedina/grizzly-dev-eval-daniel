    <footer>
        <div class="footer__inner">
            <div class="footer__left-block">
                Copyright &copy; 2014 Made by Grizzly, Inc.
            </div>
            <div class="footer__right-block">
                Right Side
            </div>
        </div>
    </footer>
    <script>
        Modernizr.load([
            {
                test: window.matchMedia,
                nope: "assets/js/inc/matchMediaAddListener.js"
            },
            {
                test: Modernizr.mq('only all'),
                nope: 'assets/js/inc/respond.min.js'
            }
        ]);
    </script>

    <script src="assets/js/inc/picturefill.min.js"></script>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/js/inc/jquery-1.10.2.min.js"><\/script>')</script>

    <script src="assets/js/plugins.js" type="text/javascript"></script>
    <script src="assets/js/main.js" type="text/javascript"></script>
</body>
</html>