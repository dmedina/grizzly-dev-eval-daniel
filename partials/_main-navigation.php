<header class="site-header">
    <div class="width-restrict">
        <a href="/" class="_logo"><span class="visuallyhidden">Grizzly</span></a>

        <nav class="m-main-navigation">
            <a data-target="menu-toggle" class="menu-toggle" href="#"><i class="icn-list"></i></a>
            <ul>
                <!-- Output like this to avoid 4px gap put in when using inline-block -->
                <li><a href="#"><i class="accent"></i>Services</a></li
                    ><li><a href="#"><i class="accent"></i>Portfolio</a></li
                    ><li><a href="#"><i class="accent"></i>Customers</a></li
                    ><li><a href="#"><i class="accent"></i>About Us</a></li
                    ><li><a href="#"><i class="accent"></i>Contact</a></li>
            </ul>
        </nav>
    </div>
</header>