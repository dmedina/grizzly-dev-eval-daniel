		<div class="flexslider  homepage-slider  js-homepage-slider">
			<ul class="slides">
				<li>
					<span data-picture data-alt="">
						<span data-src="assets/img/small.jpg"></span>
						<span data-src="http://www.fillmurray.com/1500/600" data-media="(min-width: 640px)"></span>
						<span data-src="assets/img/large.jpg" data-media="(min-width: 1500px)"></span>

						<!--[if (lt IE 9) & (!IEMobile)]>
							<span data-src="assets/img/medium.jpg"></span>
						<![endif]-->
					</span>
                    <div class="slider-caption">
                        <div class="slider-caption__align-bottom">
                            <div class="width-restrict">
                                <div class="slider-caption__content">
                                    <h2 class="slider-caption__title">Slide Headline</h2>
                                    <h3 class="slider-caption__sub-title">Slide Sub Headline</h3>
                                </div>
                            </div>
                        </div>
                    </div>
				</li>

				<li>
					<span data-picture data-alt="">
						<span data-src="assets/img/small.jpg"></span>
						<span data-src="assets/img/medium.jpg" data-media="(min-width: 640px)"></span>
						<span data-src="assets/img/large.jpg" data-media="(min-width: 1500px)"></span>

                        <!--[if (lt IE 9) & (!IEMobile)]>
							<span data-src="assets/img/medium.jpg"></span>
						<![endif]-->
					</span>
                    <div class="slider-caption">
                        <div class="slider-caption__align-bottom">
                            <div class="width-restrict">
                                <div class="slider-caption__content">
                                    <h2 class="slider-caption__title">Slide Headline</h2>
                                    <h3 class="slider-caption__sub-title">Slide Sub Headline</h3>
                                </div>
                            </div>
                        </div>
                    </div>
				</li>

				<li>
					<span data-picture data-alt="">
						<span data-src="assets/img/small.jpg"></span>
						<span data-src="assets/img/medium.jpg" data-media="(min-width: 640px)"></span>
						<span data-src="assets/img/large.jpg" data-media="(min-width: 1500px)"></span>

                        <!--[if (lt IE 9) & (!IEMobile)]>
							<span data-src="assets/img/medium.jpg"></span>
						<![endif]-->
					</span>
                    <div class="slider-caption">
                        <div class="slider-caption__align-bottom">
                            <div class="width-restrict">
                                <div class="slider-caption__content">
                                    <h2 class="slider-caption__title">Slide Headline</h2>
                                    <h3 class="slider-caption__sub-title">Slide Sub Headline</h3>
                                </div>
                            </div>
                        </div>
                    </div>
				</li>
			</ul>
            <div class="custom-control-nav"></div>
		</div>