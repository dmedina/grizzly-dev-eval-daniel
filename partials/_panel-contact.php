<section class="panel panel-contact">
    <div class="panel__inner panel-contact__inner">
        <div class="panel-contact__content">
            <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris venenatis dui quis lectus placerat rhoncus. Vivamus arcu libero, sollicitudin et condimentum a, pharetra et justo. Aliquam sollicitudin venenatis feugiat. Etiam tristique egestas ligula in rhoncus. Aliquam nec interdum nisi. Duis pharetra metus augue, ut consequat nibh malesuada id. Morbi pellentesque felis mi, non tempor diam molestie non. Aliquam tempus nulla eu elit porta aliquet.
            </p>
        </div>

        <form id="contact-form" action="" method="post" novalidate="novalidate" class="panel-contact__form">
            <div class="panel-contact__fields-col">
                <input type="text" name="contactName" id="contactName" class="required requiredField" value="" placeholder="Field One">
                <input type="text" name="email" id="email" class="required requiredField email" value="" placeholder="Field Two">
                <input type="text" name="contactName" id="contactName" class="required requiredField" value="" placeholder="Field Three">
            </div>
            <div class="panel-contact__fields-col">
                <input type="text" name="email" id="email" class="required requiredField email" value="" placeholder="Field Four">
                <input type="text" name="contactName" id="contactName" class="required requiredField" value="" placeholder="Field Five">
                <input type="text" name="email" id="email" class="required requiredField email" value="" placeholder="Field Six">
            </div>
            <div class="panel-contact__fields-col">

                <textarea name="comments" id="commentsText" class="required requiredField" cols="45" rows="8" placeholder="Message"></textarea>
                <input type="submit" name="submitted" id="submitted" value="Send Message" class="button">
            </div>
        </form>
    </div>
</section>