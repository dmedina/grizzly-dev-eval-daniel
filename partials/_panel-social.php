<section class="panel panel-social">
    <div class="panel__inner panel-social__inner">
        <div class="panel-social__block">
            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris venenatis dui quis lectus placerat rhoncus. Vivamus arcu libero, sollicitudin et condimentum a, pharetra et justo. Aliquam sollicitudin venenatis feugiat. Etiam tristique egestas ligula in rhoncus. Aliquam nec interdum nisi. Duis pharetra metus augue, ut consequat nibh malesuada id. Morbi pellentesque felis mi, non tempor diam molestie non. Aliquam tempus nulla eu elit porta aliquet.
            </p>
        </div>
        <div class="panel-social__block">
            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris venenatis dui quis lectus placerat rhoncus. Vivamus arcu libero, sollicitudin et condimentum a, pharetra et justo. Aliquam sollicitudin venenatis feugiat. Etiam tristique egestas ligula in rhoncus. Aliquam nec interdum nisi. Duis pharetra metus augue, ut consequat nibh malesuada id. Morbi pellentesque felis mi, non tempor diam molestie non. Aliquam tempus nulla eu elit porta aliquet.
            </p>
        </div>
        <div class="panel-social__block panel-social__third-block">
            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris venenatis dui quis lectus placerat rhoncus. Vivamus arcu libero, sollicitudin et condimentum a, pharetra et justo. Aliquam sollicitudin venenatis feugiat. Etiam tristique egestas ligula in rhoncus. Aliquam nec interdum nisi. Duis pharetra metus augue, ut consequat nibh malesuada id. Morbi pellentesque felis mi, non tempor diam molestie non. Aliquam tempus nulla eu elit porta aliquet.
            </p>
        </div>
        <div class="panel-social__block panel-social__social-block">
            <img src="http://www.fillmurray.com/400/400"/>
            <ul class="fa-ul">
                <li><a href="#"><i class="fa fa-facebook-square fa-3x"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram fa-3x"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin-square fa-3x"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter-square fa-3x"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus-square fa-3x"></i></a></li>
            </ul>
        </div>
    </div>
</section>